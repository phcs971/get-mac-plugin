import 'dart:async';

import 'package:flutter/services.dart';

class GetMac {
  static const MethodChannel _channel = const MethodChannel('get_mac');

  ///Gets the Mac Address of the device
  ///
  ///Works only in Android due to IOS security limitations
  ///In IOS returns the IOS Version
  ///
  ///If any error happens it throws a [PlatformException]
  static Future<String> get macAddress async {
    final String? macID = await _channel.invokeMethod<String?>('getMacAddress');
    if (macID == null) throw PlatformException;
    return macID;
  }
}
